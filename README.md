Pembelajaran pada Pertemuan 12
Dibuat oleh Izmi Habiba Kelompok FEUI04

File Studi Kasus Pertama
- labmandiri13-index.html
- labmandiri13-script.js

File Studi Kasus Kedua yang dipresentasikan
- latihan2-index.html
- latihan2-script.js

File Studi Kasus Kedua yang sudah diperbaiki dan ditambahkan CSS
- latihan2fix-index.html
- latihan2fix-script.js
- latihan2fix-style.css