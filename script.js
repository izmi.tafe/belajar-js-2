var kumpulan_buah = ["Apel", "Jambu", "Anggur", "Durian"]
console.log(kumpulan_buah[2])
console.log(kumpulan_buah.length)
console.log(kumpulan_buah)

document.getElementById("kumpulan").innerHTML = kumpulan_buah

console.log("Menambahkan Array dengan Push 2 Buah")
kumpulan_buah.push("Manggis")
kumpulan_buah.push("Nanas")
console.log(kumpulan_buah)

console.log("Menghapus array POP")
kumpulan_buah.pop();
kumpulan_buah.pop();
console.log(kumpulan_buah)

console.log("Menyisipkan dengan splice")
kumpulan_buah.splice(2, 0, "Sirsak", "Melon")
console.log(kumpulan_buah)

var harga_buah = ["1200", "2000", "2109", "1892", "2335", "3256"]
console.log(harga_buah)

console.log("Menggabungkan dengan concat")
var buah_harga = harga_buah.concat(kumpulan_buah)
console.log(buah_harga)

console.log("Memotong dengan slice")
var slice_buah = buah_harga.slice(4,8)
console.log(slice_buah)

console.log("Mengurutkan Array dengan sort, yang dibaca cuma karakter pertama")
console.log(harga_buah)
console.log(harga_buah.sort())